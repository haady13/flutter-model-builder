import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { EntityLogModel, FindDataModel, Operation } from 'mongodb-crud-operations';

@Injectable()
export class UtilService {

    private readonly bypassUrl = ["/email/receive","auth/verifyInvite.json","auth/signup.json","pendingfiles.json","/auth/signin"];

    static DataCopier(target: any, source: any): any {
        let response: any = {};
        response = { ...response, ...target };

        let keys = Object.keys(source);

        if (keys[0] == "0") {
            response = {};
        }
        else {
            for (let key of keys) {

                if (target[key]) {
                    if (target[key].constructor === Array) {
                        response[key] = [];
                        console.log(key);
                        if (source[key] != undefined) {
                            for (let item of source[key]) {
                                let data: any = { ...{}, ...item };
                                response[key].push(data);
                            }
                        }
                    }
                    else if (typeof target[key] === 'object') {
                        response[key] = source[key];
                    }
                    else {
                        if (source[key] != undefined) {
                            response[key] = source[key];
                        }
                    }
                }
                else {
                    if (source[key] != undefined) {

                        response[key] = source[key];
                    }
                }

            }
        }
        return response;

    }

    public getfindDataModelQuery(dbName: string, entityName: string, query: any) {
        let findData = new FindDataModel();
        findData.dbName = dbName;
        findData.entityName = entityName;
        findData.query = query;
        return findData;
    }

    getOTP(){
        return Math.random().toString(36).slice(-8);
    }

    public getfindDataModelId(dbName: string, entityName: string, id: string) {
        let findData = new FindDataModel();
        findData.dbName = dbName;
        findData.entityName = entityName;
        findData.id = id;
        return findData;
    }

    public getEntityLog(data: any, dbName: string, entityName: string, operation: Operation, userId: string, userName: string, entityId?: string): EntityLogModel {
        let entityLog = new EntityLogModel();
        entityLog.date = new Date().toISOString();
        entityLog.dbName = dbName;
        entityLog.entityName = entityName;
        entityLog.fieldValues = this.resolveFields(data);
        entityLog.operation = operation;
        entityLog.userId = userId;
        entityLog.userName = userName;
        entityLog.entityId = entityId;
        return entityLog;
    }

    public resolveFields(data) {
        let objectKeys = Object.keys(data);
        let fieldValues = [];
        for (let key of objectKeys) {
            let tempData = {};
            tempData[key] = data[key];
            fieldValues.push(tempData);
        }
        return fieldValues;
    }

    getPlainTextFromHtml(html: string): string {

        if ((html === null) || (html === ''))
            return html;
        else {
            html = html.toString();
        }
        html = html.replace(/(<([^>]+)>)/ig, '');
        return html.replace(/&nbsp;/g,"");
    }

    calculateBytes(x) {
        const units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        let l = 0, n = parseInt(x, 10) || 0;

        while (n >= 1024 && ++l) {
            n = n / 1024;
        }

        return (n.toFixed(n < 10 && l > 0 ? 1 : 0) + ' ' + units[l]);
    }

    formatAMPM(date) {
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
      }
}
