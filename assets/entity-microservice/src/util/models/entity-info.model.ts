export class EntityInfo {
    name: string = "";
    id: string = "";
    url?: string = "";
    type?: string = "";
}