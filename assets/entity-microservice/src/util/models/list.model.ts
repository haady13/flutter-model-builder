export class ListDTO<T>{
    list:T[];
    totalCount:number;
    enabledCount:number;
    page:number;
    count:number;
    remaining:number;
}