import { Module } from '@nestjs/common';
import { MongoBaseModule } from 'mongodb-crud-operations';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserController } from './user/user.controller';
import { UserModule } from './user/user.module';
import { UtilModule } from './util/util.module';

@Module({
  imports: [MongoBaseModule,UtilModule,UserModule],
  controllers: [AppController,UserController],
  providers: [AppService],
})
export class AppModule {}
