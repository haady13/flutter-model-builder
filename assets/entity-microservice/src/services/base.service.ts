import { Injectable } from '@nestjs/common';
import { MongoBaseService, Operation } from 'mongodb-crud-operations';
import { ResponseModel } from 'src/util/models/response.model';
import { UtilService } from 'src/util/util.service';
import { BaseEntity } from '../models/user.base.model';

@Injectable()
export class BaseService<T> {

    dbName : string = "";
    collectionName : string = "";

    constructor(
        protected readonly utilService: UtilService,
        protected readonly mongoService : MongoBaseService
    ) { }

    async findOne(id : string):Promise<ResponseModel<T>>{
        try {
            let findByIdModel = this.utilService.getfindDataModelId(this.dbName,this.collectionName,id);
            let response = await this.mongoService.findOne(findByIdModel);
            return new ResponseModel<T>(true,response,"");
        } catch (error) {
            return new ResponseModel(false,error.stack,error.message);
        }
    }

    async findMany(query:any):Promise<ResponseModel<T[]>>{
        try {
            let findModel = this.utilService.getfindDataModelQuery(this.dbName,this.collectionName,query);
            let response = await this.mongoService.findMany(findModel) as T[];
            return new ResponseModel<T[]>(true,response,"");
        } catch (error) {
            return new ResponseModel(false,error.stack,error.message);
        }
    }

    getNewObject(){
        return new BaseEntity();
    }

    async insertOne(insertEntityDTO : any):Promise<ResponseModel<T>>{
        try {
            let entity : BaseEntity = this.getNewObject();
            entity = {...entity,...insertEntityDTO};
            let insertModel = this.utilService.getEntityLog(entity,this.dbName,this.collectionName,Operation.Update,"","",Date.now().toString());
            let response = await this.mongoService.update(insertModel) as T;
            return new ResponseModel<T>(true,response,"");
        } catch (error) {
            return new ResponseModel(false,error.stack,error.message);
        }
    }

    async insertMany(insertEntityDTOs : any[]){
        try {
            throw("Method Not Implemented");
            // let entities : BaseEntity []= [];
            // for(let insertEntityDTO of insertEntityDTOs){
            //     let entity : BaseEntity = this.getNewObject();
            //     entity = {...entity,...insertEntityDTO};
            //     entities.push(entity);
            // }
            // let insertManyModel = this.utilService.getEntityLog(entities,this.dbName,this.collectionName,Operation.Create,"","");
            // // let response = await this.mongoService.insertMany(findModel) as T;
            // // return new ResponseModel<T>(true,response,"");
        } catch (error) {
            return new ResponseModel(false,error.stack,error.message);
        }

    }

    async updateOne(updateEntityDTO : any,entityId:string){
        try {
            let updateModel = this.utilService.getEntityLog(updateEntityDTO,this.dbName,this.collectionName,Operation.Update,"","",entityId);
            let response = await this.mongoService.update(updateModel) as T;
            return new ResponseModel<T>(true,response,"");
        } catch (error) {
            return new ResponseModel(false,error.stack,error.message);
        }
    }

    async updateMany(updateEntityDTO:any,query:any){
        try {
            let entityLog = this.utilService.getEntityLog(updateEntityDTO,this.dbName,this.collectionName,Operation.Update,"","","");
            entityLog.query = query;
            let response = await this.mongoService.updateMany(entityLog) as T;
            return new ResponseModel<T>(true,response,"");
        } catch (error) {
            return new ResponseModel(false,error.stack,error.message);
        }
    }

    async deleteOne(entityId:string){
        try {
            let entityLog = this.utilService.getEntityLog({},this.dbName,this.collectionName,Operation.Delete,"","",entityId);
            let response = await this.mongoService.delete(entityLog) as T;
            return new ResponseModel<T>(true,response,"");
        } catch (error) {
            return new ResponseModel(false,error.stack,error.message);
        }
    }

    async deleteMany(query){
        try {
            let deleteManyLog = this.utilService.getfindDataModelQuery(this.dbName,this.collectionName,query);
            let response = await this.mongoService.deleteMany(deleteManyLog);
            return new ResponseModel<T>(true,response,"");
        } catch (error) {
            return new ResponseModel(false,error.stack,error.message);
        }
    }

    async upsertOne(upsertEntityDTO,entityId:string){
        try {
            let entity : BaseEntity = this.getNewObject();
            entity = {...entity,...upsertEntityDTO};
            entity._id = entityId;
            let insertModel = this.utilService.getEntityLog(entity,this.dbName,this.collectionName,Operation.Update,"","");
            let response = await this.mongoService.update(insertModel) as T;
            return new ResponseModel<T>(true,response,"");
        } catch (error) {
            return new ResponseModel(false,error.stack,error.message);
        }
    }

    async upsertMany(upsertEntityDTOs : any[]){
        try {
            throw("Method Not Implemented");
            // let entities : BaseEntity []= [];
            // for(let insertEntityDTO of upsertEntityDTOs){
            //     let entity : BaseEntity = this.getNewObject();
            //     entity = {...entity,...insertEntityDTO};

            //     entities.push(entity);
            // }
            // let entityLog = this.utilService.getEntityLog(upsertEntityDTOs,this.dbName,this.collectionName,Operation.Update,"","","");
            // // let response = await this.mongoService.updateMany(entityLog) as T;
            // // return new ResponseModel<T>(true,response,"");
        } catch (error) {
            return new ResponseModel(false,error.stack,error.message);
        }
    }

}
