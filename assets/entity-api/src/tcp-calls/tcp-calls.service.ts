import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy, ClientProxyFactory, Transport } from '@nestjs/microservices';

@Injectable()
export class TcpCallsService {

    constructor(@Inject("tcp-service") private readonly tcpService: ClientProxy){};

    public async tcpCall(cmd:string,data: any) {
        try {
            return await this.tcpService.send({ cmd: cmd }, data).toPromise();
        } catch (error) {
            throw(error);
        }
    }   

}
