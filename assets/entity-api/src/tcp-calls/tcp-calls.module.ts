import { Module } from '@nestjs/common';
import { ClientProxy, ClientProxyFactory, Transport } from '@nestjs/microservices';
import { TcpCallsService } from './tcp-calls.service';

@Module({
  providers: [TcpCallsService, {
    provide: 'tcp-service',
    useFactory: () => {
      return ClientProxyFactory.create({ transport: Transport.TCP, options: { host: 'host.docker.internal', port: +"{{microServicePort}}" } })//change
    },
  }],
  exports:[TcpCallsService]
})
export class TcpCallsModule { }
