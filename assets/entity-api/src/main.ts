import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as bodyParser from 'body-parser';

async function bootstrap() {

  const app = await NestFactory.create(AppModule);
  app.use(bodyParser.json({ limit: '101mb' }));
  app.use(bodyParser.urlencoded({ limit: '101mb', extended: true }));
  app.setGlobalPrefix("{{globalPrefix}}");//change
  app.enableCors();
  var port : number = +"{{port}}";//change
  if (process.env.PORTS) {
    port = +process.env.PORTS.split(",")[0];
  }
  await app.listen(port);
  console.log("")
}
bootstrap();
