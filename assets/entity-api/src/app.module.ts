import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TcpCallsModule } from './tcp-calls/tcp-calls.module';
import { UserController } from './user/user.controller';

@Module({
  imports: [TcpCallsModule],
  controllers: [AppController,UserController],
  providers: [AppService],
})
export class AppModule {}
